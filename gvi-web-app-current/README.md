## GVI Visualizations -- Plotly x Flask

### plotly/
graphing.ipynb
- pulls data from active GVI Sharepoint
- creates graphics using Plotly
- uploads graphics to shared Plotly account
- exports graphics as iframes
- saves iframes in **../data/plotly_iframes.txt**

### data/
plotly_iframes.txt
- storage location for Plotly generated iframes
- iframes are copied manually into appropriate HTML pages

youtube_iframes.txt
- storage location for YouTube generated iframes (Goodwill promo videos)
- iframes are copied manually into appropriate HTML pages

### flask/
app.py
- runs web app
- takes HTML pages from **templates/** and renders them live
- incorporates CSS from **static/styles** and JavaScript from **static/js**
- live HTML pages include iframes stored in **../data**
