﻿using System;
using System.Security;
using Microsoft.SharePoint.Client;

namespace cSharpVis
{
    class Program
    {
        static void Main(string[] args)
        {
            string username = "skumar@goodwill-ni.org";
            var password = new SecureString();
            password.AppendChar('F');
            password.AppendChar('i');
            password.AppendChar('t');
            password.AppendChar('z');
            password.AppendChar('p');
            password.AppendChar('@');
            password.AppendChar('t');
            password.AppendChar('r');
            password.AppendChar('i');
            password.AppendChar('c');
            password.AppendChar('k');

            using (var context = new ClientContext("https://goodwillni.sharepoint.com"))
            {
                context.Credentials = new SharePointOnlineCredentials(username, password);
                context.Load(context.Web, w => w.Title);
                context.ExecuteQuery();

                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("Your site title is: " + context.Web.Title);
            }
        }

    }
}