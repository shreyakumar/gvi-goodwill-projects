# gviVisualization

## Desired filters for data:
  * Neighborhood
  * Ethnicity
  * Time (Day/Week/Month/Year)
  * Gender
  * Active vs. Inactive
  * Affiliation
  * Month of first contact
  * Probation/Parole/JJC
